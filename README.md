Zach Pedersen + Justin Dietrich's Entry for Assignment 3 - Deadlock Avoidance. Installation Steps:

Install Linux VirtualBox
Install and configure Ubuntu virtual machine
Install G++ Packages on Ubuntu machine
Take 'Deadlock.c' file from Bitbucket repository
Create a 'Deadlock.c' file in Ubuntu and paste source code
Use terminal commands "gcc -pthread Deadlock.c -o Deadlock", "./Deadlock" to compile and run

