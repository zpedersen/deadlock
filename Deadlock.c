//Zach Pedersen, Justin Dietrich
//This is our own work!
//CST-315
//Prof. Citro

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


//function to find the waiting time for processes
void findWaitingTime(int processes[], int n,
           int bt[], int wt[], int quantum)
{

//Make a copy of burst times bt[] to store remaining burst times
//Keep traversing processes until all of them are not done.
   int rem_bt[n];
   for (int i = 0 ; i < n ; i++)
       rem_bt[i] = bt[i];
   int t = 0;

   //Process timers for quitting after 3 cycles
   int timers[n];
   for (int i = 0; i < n; i++)
   {
       timers[i] = 3;
   }

   //Keeps track of which process is running
   bool using[n];
   for (int i = 0; i < n; i++)
   {
       using[i] = false;
   }

   while (1)
   {
       bool done = true;
       //Traverse all processes one by one repeatedly
       for (int i = 0 ; i < n; i++)
       {
           //If greater than 0 then only need to process further
           if (rem_bt[i] > 0)
           {
               done = false;

               //Checks to see if resource is being used
               bool inuse = false;
               for (int j = 0; j < n; j++)
               {
                   if (using[j] == true)
                   {
                       inuse = true;
                       break;
                   }
               }

               //Checks to see if resource not in use, or already using resource
               if (using[i] || !inuse)
               {
                    //Starts using resource if not in use.
                    if (!inuse)
                    {
                        using[i] = true;
                    }

                    if (rem_bt[i] > quantum)
                    {
                        //Increase and decrease the value of t by quantum
                        t += quantum;
                        rem_bt[i] -= quantum;

                        float progress = 1.0 - (float) rem_bt[i] / (float) bt[i];
                        printf("Process %i is %f percent complete.\n", i, progress);
                    }
                    //If burst time is smaller than or equal to quantum.
                    else
                    {
                        t = t + rem_bt[i];
                        wt[i] = t - bt[i];
                        rem_bt[i] = 0;
                        using[i] = false;
                        printf("Process %i done. Any other process signaled.\n", i);

                        //Reactivates temporarily terminated processes.
                        for (int j = 0; j < n; j++)
                        {
                            if (timers[j] <= 0)
                            {
                                timers[j] = 3;
                            }
                        }
                    }
               }
               //If denied resource, runs timer.
               else if (timers[i] > 0)
               {
                   t += quantum;
                   timers[i]--;
                   if (timers[i] > 0)
                   {
                       printf("Process %i denied resource.\n", i);
                   }
                   else if (timers[i] == 0)
                   {
                       printf("Process %i temporarily terminated.\n", i);
                   }
               }
           }
       }
       //When processes are done
       if (done == true)
       break;
   }
}

//Calculate turn around time
void findTurnAroundTime(int processes[], int n,
                       int bt[], int wt[], int tat[])
{
   for (int i = 0; i < n ; i++)
       tat[i] = bt[i] + wt[i];
}

// Function to calculate average time
void findavgTime(int processes[], int n, int bt[],
                                   int quantum)
{
   int wt[n], tat[n], total_wt = 0, total_tat = 0;

//Functions to find waiting and turn around time of all processes
   findWaitingTime(processes, n, bt, wt, quantum);
   findTurnAroundTime(processes, n, bt, wt, tat);

   //Display details
   printf( "Processes Burst time Waiting time Turn around time\n");

   //Calculate waiting and turn around time
   for (int i=0; i<n; i++)
   {
       total_wt = total_wt + wt[i];
       total_tat = total_tat + tat[i];
       printf(" %d \t\t %d \t %d \t\t %d",i+1, bt[i] ,wt[i] ,tat[i]);
       printf("\n");
   }
   printf("Average waiting time = %f"
       ,(float)total_wt / (float)n);
   printf("\nAverage turn around time = %f\n"
       ,(float)total_tat / (float)n);
}

int main()
{
   //IDs
   int processes[] = { 1, 2, 3};
   int n = sizeof processes / sizeof processes[0];
   //Burst time
   int burst_time[] = {10, 5, 8};
   //Quantum time
   int quantum = 2;
   findavgTime(processes, n, burst_time, quantum);
   return 0;
}